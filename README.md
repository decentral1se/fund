# The CoTech Fund

## What is the CoTech Fund?

Following a [successful pilot year](https://community.coops.tech/t/cotech-fund-pilot-year-report/1948) we have decided to make the CoTech fund permanent. The fund is a voluntary system where members of CoTech can contribute £1 per member, per week. The subscription is collected annually in advance. The fund can be used for any purpose that supports CoTech including, but not limited to:

- Marketing and promotion
- Events
- Paying for infrastructure costs (such as the [community forum](https://community.coops.tech))

Those co-ops who have joined the fund decide collectively how the fund should be distributed. We try to achieve this by consent, but where necessary can use a "one co-op, one vote" rule.

## How do I join the fund?

Send an email to `treasurer@coops.tech` and tell us how many members are in your co-op. We will then invoice you for the appropriate amount.

## Where can I find out more about the fund?

There is a [fund category](https://community.coops.tech/c/cotech/fund/25) on the community forum where we post fund updates and discuss how to use the fund. Everyone can read these updates, and members of the co-ops in the fund can post.
