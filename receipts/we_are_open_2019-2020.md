---
papersize: a4
margin-left: 20mm
margin-right: 25mm
margin-top: 10mm
margin-bottom: 20mm
...

![](img/Cotech-blue-text.png){ width=30mm }

<pre>
We Are Open
3 South Terrace
Morpeth
Northumberland
NE61 1DZ
</pre>

6th August 2019

# Receipt: CoTech Fund Contribution
## Receipt number: 00006

Dear Hannah,

Thank you for We Are Open's subscription to the CoTech fund for the period May 2019 to May 2020. On the 2nd July 2019 we received your contribution of

**£260.00**

Kind regards,

![](img/chris_lowis_signature.png){ width=33.8mm }

(Chris Lowis, Treasurer)

Chris Lowis | treasurer@coops.tech
