---
papersize: a4
margin-left: 20mm
margin-right: 25mm
margin-top: 10mm
margin-bottom: 20mm
...

![](img/Cotech-blue-text.png){ width=30mm }

<pre>
Principle Six LLP
198 Old Ford Road
London
E2 9PT
</pre>

13th June 2019

# Receipt: CoTech Fund Contribution
## Receipt number: 00003

Dear Principle Six,

Thank you for your company's subscription to the CoTech fund for the period May 2019 to May 2020. On the 9th May 2019 we received your contribution of

**£104.00**

Thank you for your contribution.

Kind regards,

![](img/chris_lowis_signature.png){ width=33.8mm }

(Chris Lowis, Treasurer)

Chris Lowis | treasurer@coops.tech
