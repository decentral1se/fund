# Administration

Records and documentation for the CoTech fund

## Generating receipts

Install `pandoc` and `wkhtmltopdf` using, for example homebrew on OS X

```
brew install pandoc
brew install Caskroom/cask/wkhtmltopdf
```

Then in `receipts/` create a markdown file from an existing one, and run, for example

```
pandoc -t html5 agile_collective_2019-2020.md -o agile_collective_2019-2020.pdf --css style.css
```



---

---

Alternatively, to make PDF invoices/receipts with markdown files [markdown themes](https://support.typora.io/About-Themes/) can be used. `theme/cotech.css` can be used as a CSS stylesheet and `invoice_template_themed.md` as template. Images path has to point to `img/` directory at the root of the repository. Invoices in `invoices/sent/2020` were created that way.

