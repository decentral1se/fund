

![](../../../img/Cotech-blue-text.png)





<pre>
Go Free Range Ltd
Lytchett House
13 Freeland Park
Wareham Road
Lytchett Matravers
Poole
Dorset
BH16 6FA
</pre>


19th June 2020



# Invoice: CoTech Fund Contribution
## Invoice number: 00016



| Description                                         | Members | Total   |
| --------------------------------------------------- | ------- | ------- |
| Annual subscription to CoTech fund @ £1/member/week | 4       | £208.00 |

Please make a payment of £208.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>
