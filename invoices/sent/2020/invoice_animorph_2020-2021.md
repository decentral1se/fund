

![](../../../img/Cotech-blue-text.png)





<pre>
Animorph 
Tower House (Outlandish)
149 Fonthill Rd
London N4 3HF
</pre>

19th June 2020



# Invoice: CoTech Fund Contribution
## Invoice number: 00013



| Description                                         | Members | Total   |
| --------------------------------------------------- | ------- | ------- |
| Annual subscription to CoTech fund @ £1/member/week | 5       | £260.00 |

Please make a payment of £260.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>
