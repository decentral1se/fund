

![](../../../img/Cotech-blue-text.png)





<pre>
Small Axe
7-15 Greatorex St
London
United Kingdom
E1 5NF
</pre>





30th July 2020



# Invoice: CoTech Fund Contribution
## Invoice number: 00026



| Description                                         | Members | Total   |
| --------------------------------------------------- | ------- | ------- |
| Annual subscription to CoTech fund @ £1/member/week | 5       | £260.00 |

Please make a payment of £260.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01



Kind regards,

![](../../../img/chris_lowis_signature.png)



(Chris Lowis, Treasurer)

<span  class="footer"> Chris Lowis | treasurer@coops.tech</span>
