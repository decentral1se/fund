---
papersize: a4
margin-left: 20mm
margin-right: 25mm
margin-top: 10mm
margin-bottom: 20mm
...

![](img/Cotech-blue-text.png){ width=30mm }

<pre>
Outlandish
Tower House
Unit 10
139-149 Fonthill Road
London
N4 3HF
</pre>

12th August 2019

# Invoice: CoTech Fund Contribution
## Invoice number: 00003

| Description                                         | Members | Total   |
|-----------------------------------------------------|---------|---------|
| Annual subscription to CoTech fund @ £1/member/week | 8       | £416.00 |

Please make a payment of £416.00 by bank transfer to:

- **Account Name**: Co-operative Technologists
- **Account Number**: 20409157
- **Sort Code**: 60-83-01

Kind regards,

![](img/chris_lowis_signature.png){ width=33.8mm }

(Chris Lowis, Treasurer)

Chris Lowis | treasurer@coops.tech
